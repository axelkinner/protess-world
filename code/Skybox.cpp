#include "Skybox.h"
#include "Constants.h"
#include "Camera.h"
#include "glm/glm.hpp"
#include "glm\gtc\type_ptr.hpp" //TODO REMOVE
#include <cassert>

#define BUFFER_OFFSET(offset) ((void*)(offset))

Skybox::Skybox(const std::string& file_path, const std::string& file_extension) :
	_vao(0),
	_cubemap(Texture(Texture::load_cube_texture(file_path, file_extension, GL_BGR, GL_RGB), GL_TEXTURE_CUBE_MAP))
{
	assert(wglGetCurrentContext() != NULL);
	
	// Box coordinates
	const GLfloat vertices[8 * 3] =
	{
		-1.0f, -1.0f, 1.0f,
		 1.0f, -1.0f, 1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, 1.0f,
		 1.0f,  1.0f, 1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f
	};

	const GLubyte indices[36] =
	{
		0, 1, 2, 0, 2, 3, // bottom
		4, 7, 6, 4, 6, 5, // top
		0, 7, 4, 0, 3, 7, // left
		1, 5, 6, 1, 6, 2, // right
		3, 2, 6, 3, 6, 7, // back
		0, 5, 1, 0, 4, 5  // front
	};

	// Initialize data on the GPU memory
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glGenBuffers(NUM_BUFFERS, _buffers);

	// Set element data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[EBO]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Set vertex data
	glBindBuffer(GL_ARRAY_BUFFER, _buffers[VBO]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(POSITION, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(POSITION);

	// Unbind buffers
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Skybox::~Skybox()
{
	glDeleteBuffers(NUM_BUFFERS, _buffers);
	glDeleteVertexArrays(1, &_vao);
}

void Skybox::draw(const Camera& camera) const
{
	// Update matrices
	const glm::mat4 matrices[1] =
	{
		// view*projection matrix (But without the camera translation).
		camera.get_projection_matrix()*camera.get_orientation_matrix(), 
	};

	glUniformMatrix4fv(MATRICES, 1, GL_FALSE, glm::value_ptr(matrices[0]));

	// Bind buffers
	glBindVertexArray(_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[EBO]);

	// Bind texture
	glActiveTexture(GL_TEXTURE0);
	_cubemap.bind();

	glDepthMask(GL_FALSE);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, NULL);
	glDepthMask(GL_TRUE);
}