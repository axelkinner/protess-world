#include "Camera.h"

Camera::Camera(const glm::vec3& position, 
	const float pitch, const float yaw,
	const float fovy, const float aspect,
	const float znear, const float zfar)
{
	set_position(position);
	set_orientation(pitch, yaw);
	set_projection(fovy, aspect, znear, zfar);
}
