#ifndef TEXTURE_H
#define TEXTURE_H

#include <Windows.h>
#include "GLEW\include\glew.h"
#include "GLFW\include\glfw3.h"
#include <string>

struct FIBITMAP;

/*
 Class to store textures in. 
 For the moment, there is only support for cube map textures.
*/

class Texture
{
public:
	// Texture becomes owner of the texture id
	explicit Texture(GLuint texID, GLenum target);
	~Texture();

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

	// Loads a cube map texture and makes it the current texture.
	// Returns the texture ID.
	static GLuint load_cube_texture(
		const std::string& file_path,
		const std::string& file_extension,
		// Does not have to be generated with glGenTextures
		GLenum image_format = GL_RGB,		// Format the image is in
		GLint internal_format = GL_RGB,		// Format to store the image in
		GLint level = 0,					// Mipmapping level
		GLint border = 0);					// Border size

	// Creates a Worley noise texture, binds it and returns the texture id
	static GLuint create_worley_texture();

	void bind() const;

private:
	// Loads the texture data and returns a pointer to it.
	static FIBITMAP* load_image_data(
		const char* filename,				// Where to load the file from
		// Does not have to be generated with glGenTextures
		GLenum image_format = GL_RGB,		// Format the image is in
		GLint internal_format = GL_RGB,		// Format to store the image in
		GLint level = 0,					// Mipmapping level
		GLint border = 0);					// Border size


	GLuint _texID;
	GLenum _target;
};

inline void Texture::bind() const
{
	glBindTexture(_target, _texID);
}


#endif