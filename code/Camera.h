#ifndef CAMERA_H
#define CAMERA_H

#include <Windows.h>
#include "GLEW\include\glew.h"
#include "GLFW\include\glfw3.h"
#include "glm/glm.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtx\rotate_vector.hpp"
#include "Constants.h"

class Camera
{
public:
	Camera(const glm::vec3& position, const float pitch = 0.0f, const float yaw = 0.0f,
		const float fovy = 60.0f, const float aspect = 1.0f, const float znear = 0.1f, const float zfar = 1000.0f);
	~Camera() = default;

	void set_position(const glm::vec3& position);
	void set_orientation(const float pitch, const float yaw);
	void set_projection(const float fovy, const float aspect, const float znear, const float zfar);

	// Increases the cameras position with local_position (defined in local coordinates).
	void add_position(const glm::vec3& local_position);

	// Increases the pitch and yaw with pitch and yaw.
	void add_orientation(const float pitch, const float yaw);

	// Returns the cameras orientation
	const glm::mat4  get_orientation_matrix() const;

	// Returns the cameras view matrix (orienation and translation)
	const glm::mat4  get_view_matrix() const;

	// Returns the projection matrix
	const glm::mat4& get_projection_matrix() const;

	// returns the cameras position
	const glm::vec3& get_pos() const;

private:
	Camera();
	Camera(const Camera&);
	Camera& operator=(const Camera&);

	// Camera params
	glm::vec3 _pos;	// Cameras position
	float _pitch;	// X-axis orientation angle
	float _yaw;		// Y-axis orientation angle

	glm::mat4 _pmatrix;
};

inline void Camera::set_position(const glm::vec3& position)
{
	_pos = position;
}

inline void Camera::set_orientation(const float pitch, const float yaw)
{
	_pitch	= glm::clamp(pitch, -89.0f,  89.0f);
	_yaw	= yaw;
}

inline void Camera::set_projection(const float fovy, const float aspect, const float znear, const float zfar)
{
	_pmatrix = glm::perspective(fovy, aspect, znear, zfar);
}

inline void Camera::add_position(const glm::vec3& local_position)
{
	set_position(_pos - glm::rotateY(glm::rotateX(local_position, -_pitch), -_yaw));
}

inline void Camera::add_orientation(const float pitch, const float yaw)
{
	set_orientation(_pitch + pitch, _yaw + yaw);
}

inline const glm::mat4 Camera::get_orientation_matrix() const
{
	return
		glm::rotate(_pitch, 1.0f, 0.0f, 0.0f)*
		glm::rotate(_yaw, 0.0f, 1.0f, 0.0f);
}

inline const glm::mat4 Camera::get_view_matrix() const
{
	return 
		get_orientation_matrix()*
		glm::translate(-_pos);
}

inline const glm::mat4& Camera::get_projection_matrix() const
{
	return _pmatrix;
}

inline const glm::vec3& Camera::get_pos() const
{ 
	return _pos; 
}

#endif