#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec3 vColor;

out VS_DATA
{
	vec3 normal;
} vs_out;

void main()
{
	gl_Position = vPosition;
	vs_out.normal = vNormal;
}