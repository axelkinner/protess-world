#include "Mesh.h"
#include "Camera.h"
#include "Constants.h"
#include "glm\gtc\type_ptr.hpp" //TODO REMOVE
#include <cassert>

#define BUFFER_OFFSET(offset) ((void*)(offset))

Mesh::Mesh(
	const std::vector<GLuint>& indices,
	const std::vector<GLfloat>& vertices,
	const std::vector<GLfloat>& normals,
	const std::vector<GLfloat>& colors) :
	_vao(0),
	_indices(indices),
	_vertices(vertices),
	_normals(normals),
	_colors(colors),
	_model_matrix(glm::mat4(1.0f))
{
	assert(wglGetCurrentContext() != NULL);

	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glGenBuffers(NUM_BUFFERS, _buffers);

	// Create element buffer object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[EBO]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size()*sizeof(GLuint), _indices.data(), GL_STATIC_DRAW);

	// Create vertex buffer objects
	glBindBuffer(GL_ARRAY_BUFFER, _buffers[VBO]);
	glBufferData(GL_ARRAY_BUFFER, (_vertices.size() + _normals.size() + _colors.size())*sizeof(GLfloat),
		NULL, GL_STATIC_DRAW);

	// Set default attributes
	glVertexAttrib3f(POSITION, 0.0f, 0.0f, 0.0f);
	glVertexAttrib3f(NORMAL, 0.0f, 1.0f, 0.0f);
	glVertexAttrib3f(COLOR, 1.0f, 1.0f, 1.0f);

	// Set vertex data
	glBufferSubData(GL_ARRAY_BUFFER, 0, _vertices.size()*sizeof(GLfloat), _vertices.data());
	glVertexAttribPointer(POSITION, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glEnableVertexAttribArray(POSITION);

	if (_normals.size() > 0)
	{
		int offset = _vertices.size()*sizeof(GLfloat);
		glBufferSubData(GL_ARRAY_BUFFER, offset, _normals.size()*sizeof(GLfloat), _normals.data());
		glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(offset));
		glEnableVertexAttribArray(NORMAL);
	}

	if (_colors.size() > 0)
	{
		int offset = (_vertices.size() + _normals.size())*sizeof(GLfloat);
		glBufferSubData(GL_ARRAY_BUFFER, offset, _colors.size()*sizeof(GLfloat), _colors.data());
		glVertexAttribPointer(COLOR, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(offset));
		glEnableVertexAttribArray(COLOR);
	}

	// Unbind buffers
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Mesh::~Mesh()
{
	glDeleteBuffers(NUM_BUFFERS, _buffers);
	glDeleteVertexArrays(1, &_vao);
}

void Mesh::draw(const GLenum mode, const Camera& camera) const
{
	// Update the matrices
	const glm::mat4 VM = camera.get_view_matrix()*_model_matrix;
	glm::mat4 matrices[4] =
	{
		VM,										// model-view matrix
		camera.get_projection_matrix(),			// projection matrix
		glm::transpose(glm::inverse(VM)),		// normal matrix
		glm::inverse(camera.get_view_matrix())  // inverse view matrix
	};
	glUniformMatrix4fv(MATRICES, 4, GL_FALSE, glm::value_ptr(matrices[0]));

	glBindVertexArray(_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[EBO]);
	glDrawElements(mode, _indices.size(), GL_UNSIGNED_INT, NULL);
}