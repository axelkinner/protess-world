#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (location = 0) in vec4 vPosition;

out vec3 tex_coord;

layout (location = 3) uniform mat4[1] MATRICES;
#define PV_MATRIX MATRICES[0]

void main()
{
	gl_Position = PV_MATRIX*vPosition;
	tex_coord = vec3(vPosition.x, -vPosition.y, vPosition.z);
}