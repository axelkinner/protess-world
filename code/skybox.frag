#version 430 core

layout (location = 12) uniform samplerCube SAMPLER;

in vec3 tex_coord;
out vec4 fcolor;

void main()
{
	fcolor = texture(SAMPLER, tex_coord);
}