#ifndef SCENE_H
#define SCENE_H

#include "Mesh.h"
#include "Skybox.h"
#include <vector>

class Camera;

/*
	Simple scene class containing a water mesh and a skybox
	The Water mesh uses the tesselation shader to dynamically create the water surface.
	The Skybox is used to draw the surrounding area with, it is also used as an environment map to the water surface.
	The camera are used to get the current view and projection matrix
*/

class Scene
{
public:
	explicit Scene(const Camera& camera, 
		const size_t water_width = 10,  // The water surface x-dim
		const size_t water_depth = 10); // The water surface z-dim
	~Scene();
	Scene(const Scene&) = delete;
	Scene& operator=(const Scene&) = delete;

	void draw(const float time) const;

private:
	enum Shaders
	{
		WATER_SHADER,
		SKYBOX_SHADER,
		NUM_SHADERS
	};

	// Creates the water mesh indices
	static std::vector<GLuint>  get_water_indices(const size_t water_width, const size_t water_depth);
	
	// Creates the water mesh vertices
	static std::vector<GLfloat> get_water_vertices(const size_t water_width, const size_t water_depth);

	const Camera& _camera;
	Mesh	_water;
	Skybox  _skybox;
	GLuint  _shaders[NUM_SHADERS];
};

#endif