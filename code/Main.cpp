#include <Windows.h>
#include "GLEW\include\glew.h"
#include "GLFW\include\glfw3.h"
#include "glm/glm.hpp"
#include <iostream>
#include "Camera.h"
#include "Scene.h"

size_t SCREEN_WIDTH  = 640;
size_t SCREEN_HEIGHT = 480;
Camera camera(glm::vec3(0, 1, 5));

// Run main loop
// 1. Update variables
// 2. Redraw scene
void main_loop(GLFWwindow* window, Scene& scene);

// Create window and set callbacks
GLFWwindow* init_window();

// Initialize OpenGL settings
void init_gl();

// Updates the view port and the cameras lens params.
void window_resize_callback(GLFWwindow *, int, int);

// Cursor callback, changes the cameras orientation
void cursor_pos_callback(GLFWwindow *, double, double);

// Moves the cameras position if wasd-keys are pressed.
void key_input(GLFWwindow* window, float dt);

// Update window title
void update_title(GLFWwindow *, float time);

int main(void)
{
	GLFWwindow* window = init_window();
	if (!window)
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}
	// enable/disable
	init_gl();

	Scene scene(camera, 30, 30);

	main_loop(window, scene);
    glfwTerminate();

    return EXIT_SUCCESS;
}

void main_loop(GLFWwindow* window, Scene& scene)
{
	float time = float(glfwGetTime());
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		float t0 = float(glfwGetTime());
		float dt = t0 - time;
		time = t0;
		update_title(window, time);

		// Input
		key_input(window, dt);

		// Render
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		scene.draw(time);
		glfwSwapBuffers(window);

		// Poll for and process events 
		glfwPollEvents();

		// Exit on escape key pressed
		if (glfwGetKey(window, GLFW_KEY_ESCAPE))
			break;
	}
}

GLFWwindow* init_window()
{
	GLFWwindow* window;

	// Initialize the library
	if (!glfwInit())
	{
		std::cout << "glfwInit failed, aborting." << std::endl;
		return NULL;
	}
		
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GL_MULTISAMPLE, 32); //TODO REMOVE

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Protess World", NULL, NULL);
	if (!window)
	{
		std::cout << "glewInit failed, aborting." << std::endl;
		return NULL;
	}

	// Make the window's context current 
	glfwMakeContextCurrent(window);

	// Set callbacks
	glfwSetWindowSizeCallback(window, window_resize_callback);
	glfwSetCursorPosCallback(window, cursor_pos_callback);

	// Hide cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	// Disable vsync
	glfwSwapInterval(1); 

	// Init glew
	glewExperimental = TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		//Problem: glewInit failed, something is seriously wrong.
		std::cout << "glewInit failed, aborting." << std::endl;
		return NULL;
	}

	printf("GL vendor:       %s\n", glGetString(GL_VENDOR));
	printf("GL renderer:     %s\n", glGetString(GL_RENDERER));
	printf("GL version:      %s\n", glGetString(GL_VERSION));

	return window;
}

void init_gl()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glClearColor(0.0, 0.0, 0.0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void window_resize_callback(GLFWwindow * window, int width, int height)
{
	SCREEN_HEIGHT = (height > 0) ? height : 1;
	SCREEN_WIDTH  = width;
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	camera.set_projection(60.0f, float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.0125f, 1024.0f);
}

void cursor_pos_callback(GLFWwindow * window, double xpos, double ypos)
{
	const double xm = SCREEN_WIDTH  >> 1;
	const double ym = SCREEN_HEIGHT >> 1;
	glfwSetCursorPos(window, xm, ym);

	const float sens = 0.1f;
	float pitch = sens*static_cast<float>(ypos - ym);
	float yaw	= sens*static_cast<float>(xpos - xm);
	camera.add_orientation(pitch, yaw);
}

void key_input(GLFWwindow* window, float dt)
{
	const float speed = 5.0f;

	glm::vec3 movement(0.0f);
	if (glfwGetKey(window, GLFW_KEY_W)) movement.z += speed*dt;
	if (glfwGetKey(window, GLFW_KEY_S)) movement.z -= speed*dt;
	if (glfwGetKey(window, GLFW_KEY_A)) movement.x += speed*dt;
	if (glfwGetKey(window, GLFW_KEY_D)) movement.x -= speed*dt;

	camera.add_position(movement);
}

float title_time = 0.0f;
unsigned int frames = 0;
void update_title(GLFWwindow *window, float time)
{
	if (time - title_time > 1.0f)
	{
		char text[30];
		sprintf(text, "Tesspro World: %i", frames);
		glfwSetWindowTitle(window, text);

		title_time = time;
		frames = 0;
	}
	
	++frames;
}