/*
* Author: Axel Kinner, axelkinner@gmail.com
*
* Vector class containing 4 float types.
* All operations use SIMD commands. (SEE, SSE2, SEE4)
* Make sure that the CPU have support for these instructions
*
* The C++ interface makes the syntax easier to read and write.
*/

#ifndef VEC4F_H
#define VEC4F_H

#include <smmintrin.h>
#include <xmmintrin.h>

class Vec4f
{
public:
	explicit Vec4f() : 
		_data(_mm_setzero_ps()) {}
	Vec4f(const float value) : 
		_data(_mm_set1_ps(value)) {}
	explicit Vec4f(const float v0, const float v1, const float v2, const float v3) :
		_data(_mm_set_ps(v3, v2, v1, v0)) {}
	explicit Vec4f(const __m128& data) :
		_data(data) {}

	/* load and store  from floats vector */

	// Load and returns 4 float from an array load_from.
	// load_from memory adress must be 16 byte aligned
	static const Vec4f load(const float* load_from)
	{
		return Vec4f(_mm_load_ps(load_from));
	}

	// Store 4 floats from load_from into the array store_in.
	// store_in memory adress must be 16 byte aligned
	static void store(const Vec4f& load_from, float* store_in)
	{
		_mm_store_ps(store_in, load_from._data);
	}

	/* Arithmetic operations */

	const Vec4f operator+(const Vec4f& rhs) const
	{
		return Vec4f(_mm_add_ps(_data, rhs._data));
	}
	const Vec4f operator-(const Vec4f& rhs) const
	{
		return Vec4f(_mm_sub_ps(_data, rhs._data));
	}
	const Vec4f operator*(const Vec4f& rhs) const
	{
		return Vec4f(_mm_mul_ps(_data, rhs._data));
	}
	const Vec4f operator/(const Vec4f& rhs) const
	{
		return Vec4f(_mm_div_ps(_data, rhs._data));
	}

	/* Logical operations */

	const Vec4f operator&(const Vec4f& rhs) const
	{
		return Vec4f(_mm_and_ps(_data, rhs._data));
	}
	const Vec4f operator|(const Vec4f& rhs) const
	{
		return Vec4f(_mm_or_ps(_data, rhs._data));
	}
	const Vec4f operator^(const Vec4f& rhs) const
	{
		return Vec4f(_mm_xor_ps(_data, rhs._data));
	}
	// returns ~lhs & rhs
	static const Vec4f and_not(const Vec4f& lhs, const Vec4f& rhs)
	{
		return Vec4f(_mm_andnot_ps(lhs._data, rhs._data));
	}


	/* Comparison operations */
	// AVX instruction _mm_cmp_ps might be better

	const Vec4f operator==(const Vec4f& rhs) const
	{
		return Vec4f(_mm_cmpeq_ps(_data, rhs._data));
	}
	const Vec4f operator<(const Vec4f& rhs) const
	{
		return Vec4f(_mm_cmplt_ps(_data, rhs._data));
	}
	const Vec4f operator>(const Vec4f& rhs) const
	{
		return Vec4f(_mm_cmpgt_ps(_data, rhs._data));
	}
	const Vec4f operator<=(const Vec4f& rhs) const
	{
		return Vec4f(_data) < Vec4f(rhs._data) | Vec4f(_data) == Vec4f(rhs._data);
	}
	const Vec4f operator>=(const Vec4f& rhs) const
	{
		return Vec4f(_data) > Vec4f(rhs._data) | Vec4f(_data) == Vec4f(rhs._data);
	}

	/* Miscellaneous */

	static const Vec4f max(const Vec4f& lhs, const Vec4f& rhs)
	{
		return Vec4f(_mm_max_ps(lhs._data, rhs._data));
	}
	static const Vec4f min(const Vec4f& lhs, const Vec4f& rhs)
	{
		return Vec4f(_mm_min_ps(lhs._data, rhs._data));
	}
	// GLSL equivalent version of mod http://www.opengl.org/sdk/docs/manglsl/xhtml/mod.xml
	static const Vec4f mod(const Vec4f& lhs, const Vec4f& rhs)
	{
		return  lhs - (rhs * floor(lhs/rhs));
	}
	// GLSL equivalent version of fract http://www.opengl.org/sdk/docs/manglsl/xhtml/floor.xml
	static const Vec4f fract(const Vec4f& rhs)
	{
		return rhs - floor(rhs);
	}

	static const Vec4f conditional_set(const Vec4f& value_if_true, const Vec4f& value_if_false, const Vec4f& condition)
	{
		return (condition & value_if_true) | and_not(condition, value_if_false);
	}

	/* Rounding */
	static const Vec4f floor(const Vec4f& rhs)
	{
		return Vec4f(_mm_floor_ps(rhs._data));
	}

private:
	__m128 _data;
};


#endif