// Slightly modified code from http://staffwww.itn.liu.se/~jimjo/courses/TNCG14-2009/
// The file contains functions for creating shader programs

#ifndef LOAD_SHADERS_H
#define LOAD_SHADERS_H

#include "GLFW\include\glfw3.h"
#include <cstdlib>
#include <fstream>

inline char* read_shader_file(char *file)
{
	if (file != NULL)
	{
		FILE *fptr;
		long length;
		char *buf;

		fptr = fopen(file, "rb"); /* Open file for reading */
		if (!fptr) /* Return NULL on failure */
			return NULL;
		fseek(fptr, 0, SEEK_END); /* Seek to the end of the file */
		length = ftell(fptr); /* Find out how many bytes into the file we are */
		buf = (char*)malloc(length + 1); /* Allocate a buffer for the entire length of the file and a null terminator */
		fseek(fptr, 0, SEEK_SET); /* Go back to the beginning of the file */
		fread(buf, length, 1, fptr); /* Read the contents of the file in to the buffer */
		fclose(fptr); /* Close the file */
		buf[length] = 0; /* Null terminator */

		return buf; /* Return the buffer */
	}
	return NULL;
}

inline GLint create_shader(char *file, GLenum type)
{
	GLint shader = glCreateShader(type);

	const char *assembly = read_shader_file(file);
	glShaderSource(shader, 1, &assembly, NULL);
	glCompileShader(shader);
	free((void *)assembly);

	GLint isCompiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);

	if (isCompiled == GL_FALSE)
	{
		char str[256];
		glGetShaderInfoLog(shader, 256, NULL, str);

		switch (type)
		{
		case GL_VERTEX_SHADER:
			fprintf(stderr, "Vertex shader compile error: %s\n", str);
			break;
		case GL_FRAGMENT_SHADER:
			fprintf(stderr, "Fragment shader compile error: %s\n", str);
			break;
		case GL_TESS_CONTROL_SHADER:
			fprintf(stderr, "Tesselation control shader compile error: %s\n", str);
			break;
		case GL_TESS_EVALUATION_SHADER:
			fprintf(stderr, "Tesselation evaluation shader compile error: %s\n", str);
			break;
		case GL_GEOMETRY_SHADER:
			fprintf(stderr, "Geometry shader compile error: %s\n", str);
			break;
		default:
			fprintf(stderr, "Shader compile error: %s\n", str);
			break;
		}
		return 0;
	}

	return shader;
}

inline GLuint link_shaders(GLuint vertex, GLuint fragment,
	GLuint control, GLuint evaluation, GLuint geometry)
{
	GLuint programObj = glCreateProgram();
	glAttachShader(programObj, vertex);
	glAttachShader(programObj, control);
	glAttachShader(programObj, evaluation);
	glAttachShader(programObj, geometry);
	glAttachShader(programObj, fragment);

	glLinkProgram(programObj);

	glDeleteShader(vertex);
	glDeleteShader(control);
	glDeleteShader(evaluation);
	glDeleteShader(geometry);
	glDeleteShader(fragment);

	GLint isLinked;
	glGetProgramiv(programObj, GL_LINK_STATUS, &isLinked);

	if (isLinked == GL_FALSE)
	{
		char str[256];
		glGetProgramInfoLog(programObj, 256, NULL, str);
		fprintf(stderr, "Program object linking error: %s\n", str);
		return 0;
	}

	                                              

	return programObj;
}


inline GLuint create_program(char *vsFile, char* fsFile, char* tcFile, char* teFile, char* gsFile)
{

	GLint vertexShader		= create_shader(vsFile, GL_VERTEX_SHADER);
	GLint fragmentShader	= create_shader(fsFile, GL_FRAGMENT_SHADER);
	GLint controlShader		= create_shader(tcFile, GL_TESS_CONTROL_SHADER);
	GLint evaluationShader	= create_shader(teFile, GL_TESS_EVALUATION_SHADER);
	GLint geometryShader	= create_shader(gsFile, GL_GEOMETRY_SHADER);

	return link_shaders(vertexShader, fragmentShader, controlShader, evaluationShader, geometryShader);
}

inline GLuint create_program(char *vsFile, char* fsFile)
{

	GLint vertexShader		= create_shader(vsFile, GL_VERTEX_SHADER);
	GLint fragmentShader	= create_shader(fsFile, GL_FRAGMENT_SHADER);

	return link_shaders(vertexShader, fragmentShader, 0, 0, 0);
}

inline GLuint create_program(char *vsFile, char* fsFile, char* tcFile, char* teFile)
{

	GLint vertexShader		= create_shader(vsFile, GL_VERTEX_SHADER);
	GLint fragmentShader	= create_shader(fsFile, GL_FRAGMENT_SHADER);
	GLint controlShader		= create_shader(tcFile, GL_TESS_CONTROL_SHADER);
	GLint evaluationShader	= create_shader(teFile, GL_TESS_EVALUATION_SHADER);

	return link_shaders(vertexShader, fragmentShader, controlShader, evaluationShader, 0);
}

inline GLuint create_program(char *vsFile, char* fsFile, char* gsFile)
{

	GLint vertexShader = create_shader(vsFile, GL_VERTEX_SHADER);
	GLint fragmentShader = create_shader(fsFile, GL_FRAGMENT_SHADER);
	GLint geometryShader = create_shader(gsFile, GL_GEOMETRY_SHADER);

	return link_shaders(vertexShader, fragmentShader, 0, 0, geometryShader);
}

#endif