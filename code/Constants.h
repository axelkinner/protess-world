#ifndef CONSTANTS_H
#define CONSTANTS_H

// Shader layout locations
enum LayoutLocation
{
	POSITION = 0,
	NORMAL = 1,
	COLOR = 2,
	MATRICES = 3,
	TIME = 11,
	SAMPLER0 = 12
};

#endif