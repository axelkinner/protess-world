// Cellular noise ("Worley noise") in 2D in GLSL.
// Copyright (c) Stefan Gustavson 2011-04-19. All rights reserved.
// This code is released under the conditions of the MIT license.
// See LICENSE file for details.

// Modified by Axel Kinner 2014-02-09
// To work with __m128 and __m256 data types.
//
// About 14 times faster using __m128 (Vec4f) and 19 times faster using __m256 (Vec8f)
// than calculate one value at the time.

#ifndef CELLULAR_H
#define CELLULAR_H

class Cellular
{
public:

	// Permutation polynomial: (34x^2 + x) mod 289
	template<class T>
	static const T permute(const T& x);

	// Cellular noise, returning F1 only.
	// Speeded up by using 2x2 search window instead of 3x3,
	// at the expense of some strong pattern artifacts.
	// F1 is sometimes wrong, too, but OK for most purposes.
	template<class T>
	static const T cellular2x2(
		const T&x,   // Vector containing horizontal positions
		const T& y); // Vector containing vertical positions
};

template<class T>
const T Cellular::permute(const T& x)
{
	return T::mod((x * 34.0f + 1.0f) * x, 289.0f);
}

template<class T>
const T Cellular::cellular2x2(const T&x, const T& y)
{
#define K 0.142857142857f	// 1/7
#define K2 0.0714285714285f // K/2
#define jitter 0.8f			// jitter 1.0 makes F1 wrong more often
	const T Pi_x = T::mod(T::floor(x), 289.0f);
	const T Pi_y = T::mod(T::floor(y), 289.0f);

	const T Pf_x = T::fract(x);
	const T Pf_y = T::fract(y);

	const T Pf_x0 = Pf_x - 0.5f;
	const T Pf_x1 = Pf_x - 1.5f;
	const T Pf_x2 = Pf_x - 0.5f;
	const T Pf_x3 = Pf_x - 1.5f;

	const T Pf_y0 = Pf_y - 0.5f;
	const T Pf_y1 = Pf_y - 0.5f;
	const T Pf_y2 = Pf_y - 1.5f;
	const T Pf_y3 = Pf_y - 1.5f;

	const T p0 = permute(permute(Pi_x) + Pi_y);
	const T p1 = permute(permute(Pi_x + 1.0f) + Pi_y);
	const T p2 = permute(permute(Pi_x) + Pi_y + 1.0f);
	const T p3 = permute(permute(Pi_x + 1.0f) + Pi_y + 1.0f);

	const T ox0 = T::mod(p0, 7.0f)*K + K2;
	const T ox1 = T::mod(p1, 7.0f)*K + K2;
	const T ox2 = T::mod(p2, 7.0f)*K + K2;
	const T ox3 = T::mod(p3, 7.0f)*K + K2;

	const T oy0 = T::mod(T::floor(p0*K), 7.0f)*K + K2;
	const T oy1 = T::mod(T::floor(p1*K), 7.0f)*K + K2;
	const T oy2 = T::mod(T::floor(p2*K), 7.0f)*K + K2;
	const T oy3 = T::mod(T::floor(p3*K), 7.0f)*K + K2;

	const T dx0 = Pf_x0 + ox0*jitter;
	const T dx1 = Pf_x1 + ox1*jitter;
	const T dx2 = Pf_x2 + ox2*jitter;
	const T dx3 = Pf_x3 + ox3*jitter;

	const T dy0 = Pf_y0 + oy0*jitter;
	const T dy1 = Pf_y1 + oy1*jitter;
	const T dy2 = Pf_y2 + oy2*jitter;
	const T dy3 = Pf_y3 + oy3*jitter;

	// d11, d12, d21 and d22, squared
	const T d0 = dx0*dx0 + dy0*dy0;
	const T d1 = dx1*dx1 + dy1*dy1;
	const T d2 = dx2*dx2 + dy2*dy2;
	const T d3 = dx3*dx3 + dy3*dy3;

	// Sort out the two smallest distances
	// Cheat and pick only F1
	return T::min(T::min(d0, d1), T::min(d2, d3));
}

#endif 