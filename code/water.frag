#version 430 core

layout (location = 3) uniform mat4[4] MATRICES;
#define INVERSE_VIEW_MATRIX mat3(MATRICES[3])

layout (location = 12) uniform samplerCube SAMPLER;

in TES_DATA
{
	vec3 normal; // normal in camera space
	vec3 pos;	 // pos in camera space
} tes_in;

out vec4 fcolor;

// Blinn phong shader (Not used at the moment)
// Can be used instead of the environment mapping
vec4 blinn_phong(const vec3 N, const vec3 V)
{
	const vec3 L = normalize(vec3(0, 1, -1)); // should be defined in view space

	const float kd = 0.3;
	const float ks = 0.6;
	const float ka = 0.05;
	const float eps = 500.0;

	// Diffuse
	float diffuse = max(0.0, dot(N, L));

	// Specular
	vec3 H = normalize(L + V);
	
	float specular = pow(max(0.0, dot(N, H)), eps);

	vec3 Lcol = vec3(1.0, 1.0, 1.0);
	vec3 Wcol = vec3(0.2, 0.3, 1.0);

	return vec4(Wcol*Lcol * (specular*ks + kd*diffuse + ka), 1.0) ;
}

float fresnel(vec3 N, vec3 V)
{
	const float F0 = 0.01657794396; // air to water reflection

	return clamp( F0 + (1.0 - F0)*pow((1.0 - dot(N,V)), 5.0), 0.0, 1.0);
}


void main()
{
	const vec3 N = normalize(tes_in.normal);

	// view direction i world space (pointing towards surface)
	const vec3 V = tes_in.pos; 

	const float R = fresnel(N, normalize(-V));
	const float T = 1.0 - R;

	vec3 tex_coord1 = INVERSE_VIEW_MATRIX*reflect(V, N);
	tex_coord1.y = -tex_coord1.y;

	// vec3 tex_coord2 = INVERSE_VIEW_MATRIX*refract(V, N, n);
	// tex_coord2.y = -tex_coord2.y;

	fcolor = vec4(
		R*texture(SAMPLER, tex_coord1).xyz + // Reflected light
		//T*texture(SAMPLER, tex_coord2).xyz   // Refracted light
		T*vec3(0.05, 0.1, 0.15)
		, 1.0);
}