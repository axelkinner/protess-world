#include "Scene.h"
#include "LoadShaders.h"
#include "Constants.h"
#include "Camera.h"
#include "glm\gtc\type_ptr.hpp" 

Scene::Scene(const Camera& camera, const size_t water_width, const size_t water_depth) :
	_camera(camera),
	_water(
		get_water_indices(water_width, water_depth), 
		get_water_vertices(water_width, water_depth)),
	_skybox("../images/skybox", ".jpg")
{
	assert(wglGetCurrentContext() != NULL);

	// Create shaders
	_shaders[WATER_SHADER] = 
		create_program(
		"../code/water.vert",
		"../code/water.frag",
		"../code/water.cont",
		"../code/water.eval");

	glUseProgram(_shaders[WATER_SHADER]);
	glUniform1i(SAMPLER0, 0); // activate sampler

	_shaders[SKYBOX_SHADER] = 
		create_program(
		"../code/skybox.vert",
		"../code/skybox.frag");

	glUseProgram(_shaders[SKYBOX_SHADER]);
	glUniform1i(SAMPLER0, 0); // activate sampler
}

Scene::~Scene()
{
	glDeleteProgram(_shaders[WATER_SHADER]);
	glDeleteProgram(_shaders[SKYBOX_SHADER]);
}

void Scene::draw(const float time) const
{
	// Draw skybox

	glUseProgram(_shaders[SKYBOX_SHADER]);
	_skybox.draw(_camera);

	// Draw water

	glUseProgram(_shaders[WATER_SHADER]);
	
	// bind skybox texture, for environment mapping
	glActiveTexture(GL_TEXTURE0);
	_skybox.get_texture().bind();

	// update misc uniforms
	glUniform1f(TIME, time);

	glPatchParameteri(GL_PATCH_VERTICES, 4);
	_water.draw(GL_PATCHES, _camera);
}

std::vector<GLuint> Scene::get_water_indices(const size_t water_width, const size_t water_depth)
{
	std::vector<GLuint> indices;
	indices.reserve((water_width - 1)*(water_depth - 1) * 4);

	for (size_t r = 0; r < water_depth - 1; ++r)
	{
		for (size_t c = 0; c < water_width - 1; ++c)
		{
			GLuint i0 = r*water_width + c;
			GLuint i1 = i0 + 1;
			GLuint i2 = i0 + water_width;
			GLuint i3 = i2 + 1;
			indices.push_back(i0);
			indices.push_back(i2);
			indices.push_back(i3);
			indices.push_back(i1);
		}
	}

	return indices;
}

std::vector<GLfloat> Scene::get_water_vertices(const size_t water_width, const size_t water_depth)
{
	GLfloat xm = float(water_width)*0.5f;
	GLfloat zm = float(water_depth)*0.5f;

	std::vector<GLfloat> vertices;
	vertices.reserve(water_width*water_depth * 3);

	for (size_t r = 0; r < water_depth; ++r)
	{
		for (size_t c = 0; c < water_width; ++c)
		{
			GLfloat x = -xm + GLfloat(c);
			GLfloat y = -0.5f;
			GLfloat z = -zm + GLfloat(r);
			vertices.push_back(x);
			vertices.push_back(y);
			vertices.push_back(z);
		}
	}

	return vertices;
}