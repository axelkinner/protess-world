/*
* Author: Axel Kinner, axelkinner@gmail.com
*
* Vector class containing 8 float types.
* All operations use SIMD commands. (AVX)
* Make sure that the CPU have support for these instructions
*
* The C++ interface makes the syntax easier to read and write.
*/


#ifndef VEC8F_H
#define VEC8F_H

#include <immintrin.h>

class Vec8f
{
public:
	explicit Vec8f() : 
		_data(_mm256_setzero_ps()) {}
	Vec8f(const float value) : 
		_data(_mm256_set1_ps(value)) {}
	explicit Vec8f(
		const float v0, const float v1, const float v2, const float v3, 
		const float v4, const float v5, const float v6, const float v7) :
		_data(_mm256_set_ps(v7, v6, v5, v4, v3, v2, v1, v0)) {}
	explicit Vec8f(const __m256& data) :
		_data(data) {}

	/* load and store  from floats vector */

	// Load and returns 8 float from an array load_from.
	// load_from memory adress must be 32 byte aligned
	static const Vec8f load(const float* load_from)
	{
		return Vec8f(_mm256_load_ps(load_from));
	}

	// Store 8 floats from load_from into the array store_in.
	// store_in memory adress must be 32 byte aligned
	static void store(const Vec8f& load_from, float* store_in)
	{
		_mm256_store_ps(store_in, load_from._data);
	}

	/* Arithmetic operations */

	const Vec8f operator+(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_add_ps(_data, rhs._data));
	}
	const Vec8f operator-(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_sub_ps(_data, rhs._data));
	}
	const Vec8f operator*(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_mul_ps(_data, rhs._data));
	}
	const Vec8f operator/(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_div_ps(_data, rhs._data));
	}

	/* Logical operations */

	const Vec8f operator&(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_and_ps(_data, rhs._data));
	}
	const Vec8f operator|(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_or_ps(_data, rhs._data));
	}
	const Vec8f operator^(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_xor_ps(_data, rhs._data));
	}
	// returns ~lhs & rhs
	static const Vec8f and_not(const Vec8f& lhs, const Vec8f& rhs)
	{
		return Vec8f(_mm256_andnot_ps(lhs._data, rhs._data));
	}


	/* Comparison operations */

	const Vec8f operator==(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_EQ_OQ));
	}
	const Vec8f operator!=(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_NEQ_OQ));
	}
	const Vec8f operator<(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_LT_OQ));
	}
	const Vec8f operator>(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_GT_OQ));
	}
	const Vec8f operator<=(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_LE_OQ));
	}
	const Vec8f operator>=(const Vec8f& rhs) const
	{
		return Vec8f(_mm256_cmp_ps(_data, rhs._data, _CMP_GE_OQ));
	}

	/* Miscellaneous */
	
	static const Vec8f max(const Vec8f& lhs, const Vec8f& rhs)
	{
		return Vec8f(_mm256_max_ps(lhs._data, rhs._data));
	}
	static const Vec8f min(const Vec8f& lhs, const Vec8f& rhs)
	{
		return Vec8f(_mm256_min_ps(lhs._data, rhs._data));
	}
	// GLSL equivalent version of mod http://www.opengl.org/sdk/docs/manglsl/xhtml/mod.xml
	static const Vec8f mod(const Vec8f& lhs, const Vec8f& rhs)
	{
		return  lhs - (rhs * floor(lhs/rhs));
	}
	// GLSL equivalent version of fract http://www.opengl.org/sdk/docs/manglsl/xhtml/floor.xml
	static const Vec8f fract(const Vec8f& rhs)
	{
		return rhs - floor(rhs);
	}

	static const Vec8f conditional_set(const Vec8f& value_if_true, const Vec8f& value_if_false, const Vec8f& condition)
	{
		return (condition & value_if_true) | and_not(condition, value_if_false);
	}

	/* Rounding */
	static const Vec8f floor(const Vec8f& rhs)
	{
		return Vec8f(_mm256_round_ps(rhs._data, 0x09));
	}

private:
	__m256 _data;
};


#endif