#ifndef MESH_H
#define MESH_H

#include <Windows.h>
#include "GLEW\include\glew.h"
#include "GLFW\include\glfw3.h"
#include "glm/glm.hpp"
#include <vector>

class Camera;

/*
	Simple class to create meshes with.
*/
class Mesh
{
public:
	// Creates an mesh from indices and vertices, the per vertex normal and color are optional.
	explicit Mesh(
		const std::vector<GLuint>&  indices,
		const std::vector<GLfloat>& vertices, 
		const std::vector<GLfloat>& normals = std::vector<GLfloat>(), 
		const std::vector<GLfloat>& colors  = std::vector<GLfloat>());
	~Mesh();

	Mesh()						 = delete;
	Mesh(const Mesh&)			 = delete;
	Mesh& operator=(const Mesh&) = delete;

	// Draws the mesh with GLenum mode, mode. The cameras is used to update the MVP matrix.
	void draw(const GLenum mode, const Camera& camera) const;

private:
	enum BUFFER_CONSTANTS {
		EBO, // element buffer object
		VBO, // vertex buffer object
		NUM_BUFFERS
	};

	GLuint _vao; // Vertex array object			
	GLuint _buffers[NUM_BUFFERS];
	std::vector<GLuint>  _indices;
	std::vector<GLfloat> _vertices;
	std::vector<GLfloat> _normals;	
	std::vector<GLfloat> _colors;

	glm::mat4 _model_matrix; // Model matrix, (identity matrix for at the moment)
};

#endif