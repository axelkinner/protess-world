#include "Texture.h"
#include "FreeImage\include\FreeImage.h"
#include "cellular.h"
#include <cassert>

Texture::Texture(GLuint texID, GLenum target) :
	_texID(texID),
	_target(target)
{
	assert(glIsTexture(_texID));
}

Texture::~Texture()
{
	glDeleteTextures(1, &_texID);
}

GLuint Texture::load_cube_texture(
	const std::string& file_path,
	const std::string& file_extension,
	GLenum image_format,
	GLint internal_format,
	GLint level,
	GLint border)
{
	assert(wglGetCurrentContext() != NULL);

	//generate an OpenGL texture ID for this texture and bind it
	GLuint gl_texID;
	glGenTextures(1, &gl_texID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, gl_texID);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	const std::string files[6] = {
		"/right",
		"/left",
		"/bottom",
		"/top",
		"/front",
		"/back"
	};

	for (size_t face = 0; face < 6; ++face)
	{
		std::string filename = (file_path + files[face] + file_extension).c_str();
		FIBITMAP * dib = load_image_data(filename.c_str(), image_format, internal_format, level, border);
		BYTE* bits = FreeImage_GetBits(dib);
		unsigned int width = FreeImage_GetWidth(dib);
		unsigned int height = FreeImage_GetHeight(dib);

		// Store the texture data for OpenGL use
		GLenum target = GL_TEXTURE_CUBE_MAP_POSITIVE_X + face;
		glTexImage2D(target, level, internal_format, width, height,
			border, image_format, GL_UNSIGNED_BYTE, bits);

		//Free FreeImage's copy of the data
		FreeImage_Unload(dib);
	}

	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	
	//return id
	return gl_texID;
}

// TODO create the texture using FBO or by using simds units instead
// The worley noise is quite slow and uses double instead of float.
static GLuint create_worley_texture()
{

}

FIBITMAP * Texture::load_image_data(
	const char* filename,
	GLenum image_format,
	GLint internal_format,
	GLint level,
	GLint border)
{
	// Original author Ben English
	// benjamin.english@oit.edu
	//
	// For use with OpenGL and the FreeImage library

	//image format
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	//pointer to the image, once loaded
	FIBITMAP *dib(0);
	//pointer to the image data
	BYTE* bits(0);
	//image width and height
	unsigned int width(0), height(0);

	//check the file signature and deduce its format
	fif = FreeImage_GetFileType(filename, 0);
	//if still unknown, try to guess the file format from the file extension
	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(filename);
	//if still unkown, throw exception
	if (fif == FIF_UNKNOWN)
		throw "Image file format are unknown!";

	//check that the plugin has reading capabilities and load the file
	if (FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, filename);
	//if the image failed to load, throw exception
	if (!dib)
		throw "Image failed to load!";

	// Check if we can retrieve the image data
	bits = FreeImage_GetBits(dib);
	width = FreeImage_GetWidth(dib);
	height = FreeImage_GetHeight(dib);
	//if this somehow one of these failed (they shouldn't), throw exception
	if ((bits == 0) || (width == 0) || (height == 0))
	{
		FreeImage_Unload(dib);
		throw "Failed to retrieve internal data from image!";
	}

	return dib;
}