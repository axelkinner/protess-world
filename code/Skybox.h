#ifndef SKYBOX_H
#define SKYBOX_H

#include <Windows.h>
#include "GLEW\include\glew.h"
#include "GLFW\include\glfw3.h"
#include "Texture.h"
#include <string>

class Camera;

/*
	Simple class to create skyboxes with.
	The class should be drawn first of all objects within the scene.
*/

class Skybox
{
public:
	// Creates an skybox from the images front, back, top, bottom, right, left
	// in the directory at file_path. The images has the file extension file_extension.
	explicit Skybox(const std::string& file_path, const std::string& file_extension);
	~Skybox();

	Skybox() = delete;
	Skybox(const Skybox&) = delete;
	Skybox& operator=(const Skybox&) = delete;

	void draw(const Camera& camera) const;
	const Texture& get_texture() const;

private:
	enum BUFFER_CONSTANTS {
		EBO, // element buffer object
		VBO, // vertex buffer object
		NUM_BUFFERS
	};

	GLuint _vao; // vertex buffer object
	GLuint _buffers[NUM_BUFFERS];

	const Texture _cubemap;
};	

inline const Texture& Skybox::get_texture() const
{
	return _cubemap;
}

#endif